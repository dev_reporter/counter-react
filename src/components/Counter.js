import React from 'react';
import { connect } from 'react-redux';
import { setCount } from '../actions';
import rebase from '../databases/Rebase';
import store from '../databases/Redux';

class Counter extends React.Component {
    componentDidMount() {
        rebase.listenTo('counter/count', {
            context: this,
            asArray: false,
            then(count){
                store.dispatch(setCount(count))
            }
        })
    }

    render() {

        return (
            <h1>Count: {this.props.count}</h1>
        );
    }
}

let mapStateToProps = (state) => {
    return {
        count: state.counter.count
    };
}

Counter = connect(mapStateToProps)(Counter);

export default Counter;
