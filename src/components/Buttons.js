import React from 'react';
import { connect } from 'react-redux';
import { increment, decrement } from '../actions';
import rebase from '../databases/Rebase';
import store from '../databases/Redux';

class Buttons extends React.Component {
    render() {
        return (
            <div>
                <button type="button"
                        onClick={ this.props.onIncrement }>
                        +
                </button>
                <button type="button"
                        onClick={ this.props.onDecrement }>
                        -
                </button>
            </div>
        )
    }
}

let mapDispatchToProps = (dispatch) => {
    return {
        onIncrement: () => {
            rebase.update('counter', {
                data: {count: store.getState().counter.count+store.getState().counter.diff}
            }).then(() => {
                // dispatch(increment());
            }).catch(err => {
                // handle error
            }); 
        },
        onDecrement: () => {
            rebase.update('counter', {
                data: {count: store.getState().counter.count-store.getState().counter.diff}
            }).then(() => {
                // dispatch(decrement());
            }).catch(err => {
                // handle error
            }); 
            dispatch(decrement());
        }
    }
}   
 
// let mapDispatchToProps = (dispatch) => {
//     return {
//         onIncrement: () => dispatch(increment()),
//         onDecrement: () => dispatch(decrement())
//     }
// }
 
Buttons = connect(undefined, mapDispatchToProps)(Buttons);
 
export default Buttons;