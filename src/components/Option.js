import React from 'react';
import { connect } from 'react-redux';
import { setDiff } from '../actions';
import rebase from '../databases/Rebase';
import store from '../databases/Redux';
 
class Option extends React.Component {
    constructor(props) {
        super(props);
 
        this.state = {
            diff: '1'
        }
 
        this.onChangeDiff = this.onChangeDiff.bind(this);
    }

    componentDidMount(){
        rebase.listenTo('counter/diff', {
            context: this,
            asArray: false,
            then(diff){
                store.dispatch(setDiff(diff));
            }
        })
    }
 
    render() {
        return (
            <div>
                <input type="text" value={ this.props.diff } onChange={ this.onChangeDiff }></input>
            </div>
        );
    }
 
    onChangeDiff(e) {
 
        if(isNaN(e.target.value))
            return;
 
        if(e.target.value=='') {
            rebase.update('counter', {
                data: {diff: 0}
            }).then(() => {
                // dispatch(increment());
            }).catch(err => {
                // handle error
            }); 
        }

        this.props.onUpdateDiff(parseInt(e.target.value)); 
    }
}
 
let mapDispatchToProps = (dispatch) => {
    return {
        onUpdateDiff: (value) => dispatch(setDiff(value))
    };
}

let mapStateToProps = (state) => {
    return {
        diff: state.counter.diff
    };
}
 
Option = connect(mapStateToProps, mapDispatchToProps)(Option);
 
export default Option;