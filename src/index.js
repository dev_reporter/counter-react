import React from 'react';
import ReactDOM from 'react-dom';
import { Provider  } from 'react-redux';
import App from './components/App';
import store from './databases/Redux';
import { setDiff, setCount } from './actions';


const appElement = document.getElementById('root');

ReactDOM.render(
    <Provider store = {store}>
        <App />
    </Provider>,
    appElement
);
