// webpack.config.js
module.exports = {
  entry: './src/index.js',
  output: {
    path: __dirname + '/public/',
    filename: 'bundle.js'
  },
  devServer: {
        inline: true,
        port: 7777,
        contentBase: __dirname + '/public/'
  },
  module: {
    loaders: [
      {
        test: /\.sass$/, loader: 'style!css!sass' 
      },
      {
        test: /\.js$/,
        loader: 'babel',
        exclude: /node_modules/,
        query: {
            cacheDirectory: true,
            presets: ['es2015', 'react']
        }
      }
    ]
  }
};